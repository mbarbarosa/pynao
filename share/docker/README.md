# Docker images: How to

## Build and push the image on gitlab

    docker login registry.gitlab.com
    docker build -t registry.gitlab.com/mbarbry/pynao .
    docker push registry.gitlab.com/mbarbry/pynao

For login, you may need to use a Personal Access Token, see
https://gitlab.com/help/user/profile/personal_access_tokens

## Build docker

    docker build --tag pynao .

## Run docker interactively

    docker run -it pynao bash
