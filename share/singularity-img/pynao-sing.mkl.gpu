Bootstrap: docker
From: ubuntu:18.04

%runscript
  exec /opt/conda/bin/python "$@"

%apprun notebook
  exec /opt/conda/bin/jupyter notebook --notebook-dir=/dataSingularity

%help
  try running with run script_name or --app notebook
  To change the number of thread to use inside python use
  import os
  os.environ["NUMTHREADS"] = "number threads"



%environment
  export PATH="/opt/conda/bin:$PATH"              # Python anaconda
  export XDG_RUNTIME_DIR=$HOME                    # to use jupyter notebook
  export PATH="/opt/siesta/Obj:$PATH"             # Siesta
  export NUMTHREADS="$(nproc --all)"
  export ASE_SIESTA_COMMAND="siesta < PREFIX.fdf > PREFIX.out"
  export LD_LIBRARY_PATH=/opt/conda/lib/mkl/lib:${LD_LIBRARY_PATH}
  export SIESTA_PP_PATH=/opt/pynao/share/pseudo
  export PYTHONPATH=/opt/pyscf:$PYTHONPATH
  export LD_LIBRARY_PATH=/opt/pyscf/pyscf/lib/deps/lib:${LD_LIBRARY_PATH}
# pyscf needs the following variable to be able to use multithreading with BLAS
  export MKL_NUM_THREADS="$(nproc --all)"

%labels
  AUTHOR marc.barbry@mailoo.org

%post
  echo "Hello from inside the container"
  echo "The post section is where you can install, and configure your container."
  apt-get update && apt-get -y install git wget curl htop gcc gfortran vim build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev nvidia-cuda-toolkit nvidia-utils-430
  
  wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh -P /opt
  /bin/bash /opt/Anaconda3-2019.10-Linux-x86_64.sh -b -p /opt/conda
  rm /opt/Anaconda3-2019.10-Linux-x86_64.sh
  mkdir -p /opt/conda/lib/mkl/lib && ln -s /opt/conda/lib/libmkl* /opt/conda/lib/mkl/lib/
  export PATH="/opt/conda/bin:$PATH"
  conda update conda
  conda install plotly
  conda install cupy
  pip install ase

  export CC=gcc && export FC=gfortran && export CXX=g++
  
  #conda install -c pyscf pyscf
  cd /opt && git clone https://github.com/pyscf/pyscf.git && cd pyscf
  git fetch && git checkout master && git pull
  cd pyscf/lib && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make
  export PYTHONPATH=/opt/pyscf:$PYTHONPATH
  export LD_LIBRARY_PATH=/opt/pyscf/pyscf/lib/deps/lib:${LD_LIBRARY_PATH}

  cd /opt && git clone https://gitlab.com/mbarbry/pynao.git && cd pynao
  git fetch && git checkout master
  cp lib/cmake_user_inc_examples/cmake.user.inc-singularity.anaconda.gnu.mkl lib/cmake.arch.inc
  python setup.py bdist_wheel
  cd dist && pip install pynao-0.1.0-py3-none-any.whl

  cd /opt/pynao/share/pseudo && tar -xzf pseudos.tar.gz

  cd /opt && git clone https://gitlab.com/siesta-project/siesta.git
  cd siesta/Obj && sh ../Src/obj_setup.sh
  cp /opt/pynao/share/siesta-arch.make/arch.make.mkl.openmp arch.make
  make -j 4

  mkdir -p /dataSingularity
  chmod 777 -R /dataSingularity
  chmod 777 -R /opt/conda
