Bootstrap: docker
From: ubuntu:18.04

%help
  try running with run script_name or --app notebook
  To change the number of thread to use inside python use
  import os
  os.environ["NUMTHREADS"] = "number threads"



%environment
  export XDG_RUNTIME_DIR=$HOME                    # to use jupyter notebook
  export PATH="/opt/siesta/Obj:$PATH"             # Siesta
  export NUMTHREADS="$(nproc --all)"
  export ASE_SIESTA_COMMAND="siesta < PREFIX.fdf > PREFIX.out"
  export SIESTA_PP_PATH=/opt/pynao/share/pseudo
# pyscf needs the following variable to be able to use multithreading with BLAS
  export MKL_NUM_THREADS="$(nproc --all)"

%labels
  AUTHOR marc.barbry@mailoo.org

%post
  echo "Hello from inside the container"
  echo "The post section is where you can install, and configure your container."
  apt-get update && apt-get -y install git wget curl htop gcc gfortran vim python3 python3-pip build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev
  
  pip3 install numpy scipy matplotlib numba ase pyscf
  
  export CC=gcc && export FC=gfortran && export CXX=g++
  cd /opt && git clone https://gitlab.com/mbarbry/pynao.git && cd pynao
  git fetch && git checkout master
  cp lib/cmake_user_inc_examples/cmake.user.inc-gnu lib/cmake.arch.inc
  python3 setup.py bdist_wheel
  cd dist && pip3 install pynao-0.1.0-py3-none-any.whl

  cd /opt/pynao/share/pseudo && tar -xzf pseudos.tar.gz

  cd /opt && git clone https://gitlab.com/siesta-project/siesta.git
  cd siesta/Obj && sh ../Src/obj_setup.sh
  sed -e 's/LIBS =/LIBS = -lblas -llapack/; s/COMP_LIBS = -lblas -llapack libsiestaLAPACK.a libsiestaBLAS.a/COMP_LIBS =/' gfortran.make > arch.make
  make -j 4
 
  mkdir -p /dataSingularity
  chmod 777 -R /dataSingularity
