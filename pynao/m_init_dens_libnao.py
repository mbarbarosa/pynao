from __future__ import print_function, division
from pynao.m_libnao import lib_nao
from ctypes import POINTER, c_int64, byref

lib_nao.init_dens_libnao.argtypes = ( POINTER(c_int64), ) # info

def init_dens_libnao():
  """ Initilize the auxiliary for computing the density on libnao site """

  info = c_int64(-999)
  lib_nao.init_dens_libnao( byref(info))

  return info.value
