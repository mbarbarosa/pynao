from __future__ import division
import numpy as np
from ctypes import POINTER, c_double, c_int, c_float
from pynao.m_libnao import lib_sparsetools


def spmv_wrapper(n, alpha, ap, x, beta = 0.0, incx = 1, incy = 1, lower=0):
  """ Small blas wrapper that is missing in scipy.linalg.blas in scipy.version<=1 """

  if ap.size != n*(n+1)//2:
    raise ValueError("simple wrapper, you MUST provide x.size = n, ap.size = n*(n+1)/2")
    
  if ap.dtype == np.float32:
    y = np.zeros((n), dtype=np.float32)
    lib_sparsetools.SSPMV_wrapper(c_int(lower), c_int(n), c_float(alpha),
                ap.ctypes.data_as(POINTER(c_float)),
                x.ctypes.data_as(POINTER(c_float)), c_int(incx), c_float(beta),
                y.ctypes.data_as(POINTER(c_float)), c_int(incy))
  elif ap.dtype == np.float64:
    y = np.zeros((n), dtype=np.float64)
    lib_sparsetools.DSPMV_wrapper(c_int(lower), c_int(n), c_double(alpha),
                ap.ctypes.data_as(POINTER(c_double)),
                x.ctypes.data_as(POINTER(c_double)), c_int(incx), c_double(beta),
                y.ctypes.data_as(POINTER(c_double)), c_int(incy))
  else:
    raise ValueError("dtype error, only np.float32 and np.float64 implemented")

  return y
