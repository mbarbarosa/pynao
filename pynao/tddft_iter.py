from __future__ import print_function, division
import sys
from copy import copy
import numpy as np
from numpy import require, zeros_like
from scipy.linalg import blas

from timeit import default_timer as timer

from pyscf.data.nist import HARTREE2EV
from pynao.chi0_matvec import chi0_matvec
from pynao.m_pack2den import pack2den_u
from pynao.m_kernel_utils import kernel_initialization
    
class tddft_iter(chi0_matvec):
    """ 
    Iterative TDDFT a la PK, DF, OC JCTC

    Input Parameters:
    -----------------
    """

    def __init__(self, **kw):

        load_kernel = kw['load_kernel'] if 'load_kernel' in kw else False
        self.res_method = kw["res_method"] if "res_method" in kw else "both"

        # better to check input before to initialize calculations
        chi0_matvec.__init__(self, **kw)
        if self.scipy_ver < 1 and self.res_method != "both":
            import warnings
            warnings.warn("scipy.__version__ < 1, the res_method both will be used!")

        self.xc_code_mf = copy(self.xc_code)
        # xc_code must be in kw ...
        if "xc_code" in kw.keys():
            self.xc_code = kw['xc_code']
        else:
            kw['xc_code'] = self.xc_code

        self.kernel, self.kernel_dim, self.ss2kernel = \
                kernel_initialization(self, **kw)

        if self.verbosity > 0:
            print(__name__,'\t====> self.xc_code:', self.xc_code)

        #self.preconditionner = self.tddft_iter_preconditioning()

    def comp_fxc_lil(self, **kw):
        """
        Computes the sparse version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_lil import vxc_lil
        return vxc_lil(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_fxc_pack(self, **kw):
        """
        Computes the packed version of the TDDFT interaction kernel
        """
        from pynao.m_vxc_pack import vxc_pack
        return vxc_pack(self, deriv=2, ao_log=self.pb.prod_log, **kw)

    def comp_veff(self, vext, comega=1j*0.0, x0=None):
        """
        This computes an effective field (scalar potential) given the external
        scalar potential
        """
        import scipy.sparse.linalg as splin

        nsp = self.nspin*self.nprod

        assert len(vext) == nsp, "{} {}".format(len(vext), nsp)
        self.comega_current = comega
        veff_op = splin.LinearOperator((nsp,nsp), matvec=self.vext2veff_matvec,
                                 dtype=self.dtypeComplex)

        b = np.require(vext, dtype=self.dtypeComplex, requirements='C')
        resgm, info = self.krylov_solver(veff_op, b, x0=x0, **self.krylov_options)

        if info != 0:
            print("Krylov Warning: info = {0}".format(info))
    
        return resgm

    def tddft_iter_preconditioning(self):

        import scipy.sparse as sp

        nsp = self.nspin*self.nprod
        data = np.zeros((nsp), dtype=self.dtype)
        data.fill(1.0)

        rowscols = np.arange(nsp, dtype=np.int32)
        M = sp.coo_matrix((data, (rowscols, rowscols)), shape=(nsp, nsp),
                          dtype=self.dtype)
        return M.tocsr()

    def vext2veff_matvec(self, vin):
        self.matvec_ncalls += 1
        dn0 = self.apply_rf0(vin, self.comega_current)
        vcre, vcim = self.apply_kernel(dn0)
        return vin - (vcre + 1.0j*vcim)
  
    def vext2veff_matvec2(self, vin):
        self.matvec_ncalls+=1
        dn0 = self.apply_rf0(vin, self.comega_current)
        vcre, vcim = self.apply_kernel(dn0)
        return 1 - (vin - (vcre + 1.0j*vcim))

    def apply_kernel(self, dn):
        if self.nspin==1:
            return self.apply_kernel_nspin1(dn)
        elif self.nspin==2:
            return self.apply_kernel_nspin2(dn)

    def apply_kernel_nspin1(self, dn):
    
        daux  = np.zeros(self.nprod, dtype=self.dtype)
        daux[:] = require(dn.real, dtype=self.dtype, requirements=["A","O"])
        vcre = self.spmv(self.nprod, 1.0, self.kernel, daux)
        
        daux[:] = require(dn.imag, dtype=self.dtype, requirements=["A","O"])
        vcim = self.spmv(self.nprod, 1.0, self.kernel, daux)
        return vcre, vcim

    def apply_kernel_nspin2(self, dn):

        vcre = np.zeros((2,self.nspin,self.nprod), dtype=self.dtype)
        daux = np.zeros((self.nprod), dtype=self.dtype)
        s2dn = dn.reshape((self.nspin,self.nprod))

        for s in range(self.nspin):
            for t in range(self.nspin):
                for ireim,sreim in enumerate(('real', 'imag')):
                    daux[:] = require(getattr(s2dn[t], sreim),
                                      dtype=self.dtype, requirements=["A","O"])
                    vcre[ireim,s] += self.spmv(self.nprod, 1.0, self.ss2kernel[s][t], daux)

        return vcre[0].reshape(-1),vcre[1].reshape(-1)

    def comp_polariz_nonin_xx(self, comegas, tmp_fname=None):
        """
        Compute the non-interacting polarizability along the xx direction
        """
        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)
        return self.p0_mat[0, 0, :]

    def comp_polariz_inter_xx(self, comegas, tmp_fname=None):
        """
        Compute the interacting polarizability along the xx direction
        """
        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 0.0, 0.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)
        return self.p_mat[0, 0, :]

    def comp_polariz_nonin_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)

        Pavg = np.zeros((self.p0_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p0_mat[i, i, :]

        return Pavg/3

    def comp_polariz_inter_ave(self, comegas, tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)

        Pavg = np.zeros((self.p_mat.shape[2]), dtype=self.dtypeComplex)
        for i in range(3):
            Pavg[:] += self.p_mat[i, i, :]

        return Pavg/3
    polariz_inter_ave = comp_polariz_inter_ave

    def comp_polariz_nonin_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn0, self.p0_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=False)

        return self.p0_mat

    def comp_polariz_inter_Edir(self, comegas, Eext=np.array([1.0, 1.0, 1.0]),
                                tmp_fname=None):
        """
        Compute average interacting polarizability
        """

        self.dn, self.p_mat = \
                self.comp_dens_along_Eext(comegas,
                                          Eext=np.array([1.0, 1.0, 1.0]),
                                          tmp_fname=tmp_fname,
                                          inter=True)

        return self.p_mat
