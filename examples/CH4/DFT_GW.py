"""
Run DFT+GW calculation on one go.
DFT calulation is first done using Siesta via the ASE wrapper.
Then GW calculations are performed with pynao
"""
import os
import numpy as np

import ase.io as io
from ase.calculators.siesta import Siesta
from ase.units import Ry, eV, Ha
from pynao import gw_iter

# First run DFT calculation with Siesta

dname = os.getcwd() 
fname = "CH4.xyz"

# load the molecule geometry
atoms = io.read(fname)

# set-up the Siesta parameters
siesta = Siesta(
      mesh_cutoff=250 * Ry,
      basis_set='DZP',
      pseudo_qualifier='gga',
      xc="PBE",
      energy_shift=(25 * 10**-3) * eV,
      fdf_arguments={
        'SCFMustConverge': False,
        'COOP.Write': True,
        'WriteDenchar': True,
        'PAO.BasisType': 'split',
        'DM.Tolerance': 1e-4,
        'DM.MixingWeight': 0.01,
        "MD.NumCGsteps": 0,
        "MD.MaxForceTol": (0.02, "eV/Ang"),
        'MaxSCFIterations': 10000,
        'DM.NumberPulay': 4,
        'XML.Write': True,
        "WriteCoorXmol": True})

atoms.set_calculator(siesta)

# Run Siesta calculation
e = atoms.get_potential_energy()
print("DFT potential energy", e)


# Then run GW calculation with pynao
gw = gw_iter(label='siesta', cd=dname, verbosity=8, rescf=True, niter_max_ev=50,
             tol_ev=1.0e-3, use_initial_guess_ite_solver=True, SCF_kernel_conv_tol=1.0e-8,
             krylov_solver="bicgstab", krylov_options={"tol": 1.0e-5, "atol": 1.0e-8},
             GPU=False, dtype=np.float32, gw_xvx_algo="dp_sparse")
gw.kernel_gw_iter()
gw.report()
