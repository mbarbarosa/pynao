from __future__ import print_function, division
import unittest, numpy as np
from pyscf import gto, scf
from pynao import gw as gw_c

mol = gto.M(verbose=1, atom='''Be 0.0 0.0 0.269654; H 0.0 0.0 -1.078616''', 
            basis='cc-pvdz', spin=1)

gto_mf_uhf = scf.UHF(mol)
e_tot = gto_mf_uhf.kernel()

class KnowValues(unittest.TestCase):

    def test_beh_gw_0088(self):
        from pynao.m_fermi_dirac import fermi_dirac_occupations
        """
        Spin-resolved case GW procedure
        """
        gw = gw_c(mf=gto_mf_uhf, gto=mol, verbosity=0, niter_max_ev=8, pb_algorithm='pp')
        self.assertEqual(gw.nspin, 2)

        gw.kernel_gw()
        self.assertAlmostEqual(gw.mo_energy_gw[0,0,2]*27.2114, -8.4558357834412305)

if __name__ == "__main__":
    unittest.main()
