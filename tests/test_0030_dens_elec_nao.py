# Copyright 2014-2018 The PySCF Developers. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf

class KnowValues(unittest.TestCase):

    def test_dens_elec(self):
        """
        Compute density in coordinate space with scf, integrate and compare with
        number of electrons
        """
        from timeit import default_timer as timer
        
        sv = mf(label='water', cd=os.path.dirname(os.path.abspath(__file__)))
        dm = sv.make_rdm1()
        grid = sv.build_3dgrid_pp(level=5)
        dens = sv.dens_elec(grid.coords, dm)
        nelec = np.einsum("is,i", dens, grid.weights)[0]
        self.assertAlmostEqual(nelec, sv.hsx.nelec, 2)

if __name__ == "__main__":
    unittest.main()
