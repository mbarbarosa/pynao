from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf as mf_c

class KnowValues(unittest.TestCase):

    def test_vneutral_atom_matrix_elements(self):
        """
        reSCF then G0W0
        """

        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='n2', cd=dname)
        vna = mf.vna_coo(level=1).toarray()
        rdm = mf.make_rdm1()[0,0,:,:,0]
        Ena = 27.2114*(-0.5)*(vna*rdm).sum()
        self.assertAlmostEqual(Ena, 133.24203392039948)

if __name__ == "__main__":
    unittest.main()
