from __future__ import print_function, division
import os, unittest, numpy as np

from pynao import mf as mf_c
from pynao.m_ao_eval import ao_eval
    
class KnowValues(unittest.TestCase):

    def test_ao_eval_speed(self):
        """
        Test the computation of atomic orbitals in coordinate space
        """
        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(verbosity=0, label='water', cd=dname, gen_pb=False,
                  force_gamma=True, Ecut=20)
        g = mf.mesh3d.get_3dgrid()
        oc2v1 = mf.comp_aos_den(g.coords)
        oc2v2 = mf.comp_aos_py(g.coords)
        self.assertTrue(np.allclose(oc2v1, oc2v2, atol=3.5e-5))

if __name__ == "__main__":
    unittest.main()
