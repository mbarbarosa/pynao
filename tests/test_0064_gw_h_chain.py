from __future__ import print_function, division
import unittest, numpy as np
from pyscf import gto, scf
from pynao import gw

atoms = '''H 0 0 0;  H 0 0 0.5; H 0 0 1.0; H 0 0 1.5; H 0 0 2.0; H 0 0 2.5; H 0 0 3.0; H 0 0 3.5;'''
class KnowValues(unittest.TestCase):

    def test_gw_h2_ae_spin_rf0_speed(self):
        """
        This is GW
        """
        mol = gto.M(verbose=1, atom=atoms, basis='cc-pvdz', spin=0)
        gto_mf = scf.RHF(mol)
        etot = gto_mf.kernel()
        b = gw(mf=gto_mf, gto=mol, verbosity=0, nvrt=4)
        ww = np.arange(0.0, 1.0, 0.1)+1j*0.2
        rf0 = b.rf0(ww)
        rf0_ref = b.rf0_cmplx_ref(ww)
        self.assertTrue(abs(rf0_ref-rf0).sum()/rf0.size<1e-11)

if __name__ == "__main__":
    unittest.main()
